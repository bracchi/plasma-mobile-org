---
title: Join Plasma Mobile
permalink: /join/
layout: default
---

![Konqi](/img/424px-Mascot_konqi-app-internet.png){:width="200"}

If you'd like to contribute to the amazing free software for mobile devices, [join us - we always have a task for you](/contributing/)! 

Plasma Mobile community groups and channels:

### Plasma Mobile specific channels:

* [![](/img/telegram.svg){:width="30"} Telegram](https://t.me/plasmamobile)

* [![](/img/matrix.svg){:width="30"} Matrix](https://matrix.to/#/#plasmamobile:matrix.org)

The Telegram and Matrix channels are bridged, so you don't have to join both

* [![](/img/mail.svg){:width="30"} Plasma Mobile mailing list](https://mail.kde.org/mailman/listinfo/plasma-mobile)

### Plasma Mobile and other topics:

* [![](/img/irc.png){:width="30"} IRC #plasma on Freenode](https://kiwiirc.com/nextclient/chat.freenode.net/#plasma)

* [![](/img/telegram.svg){:width="30"} Telegram #Halium](https://t.me/Halium)

* [![](/img/mail.svg){:width="30"} Plasma development mailing list](https://mail.kde.org/mailman/listinfo/plasma-devel)
