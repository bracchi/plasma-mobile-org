---
title: Screenshots
permalink: /screenshots/
layout: default
---

![plasma-screen-1](/img/plasma-screen-1.png){:width="45%"}
![plasma-screen-7](/img/plasma-screen-7.png){:width="45%"}

![plasma-screen-5](/img/plasma-screen-5.png){:width="45%"}
![plasma-screen-4](/img/plasma-screen-4.png){:width="45%"}
